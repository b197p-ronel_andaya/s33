console.log('hello world');
// 3

async function getTodos(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const resJson = await response.json();
    console.log(resJson);
    // 4
    const allTitles = resJson.map((json)=> 'title: '+ json.title);
    console.log(allTitles);
    
}
getTodos();

// 5
fetch('https://jsonplaceholder.typicode.com/todos/103')
.then((res)=>res.json())
.then((json)=>console.log(json));

//6
fetch('https://jsonplaceholder.typicode.com/todos/103')
.then((res)=>res.json())
.then((json)=>{
    console.log(`title: ${json.title} | completed: ${json.completed}`);
});

//7
async function createTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos',
    {
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            userId: 6,
            completed: true,
            title: "New Post"
        })
    });
    
    const resJson = await response.json();
    console.log(resJson);
}
createTodo();

//8
async function updateTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is an updated to do"
        })
    });
    
    const resJson = await response.json();
    console.log(resJson);
}
updateTodo();

//9
async function updateTodoStructure(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
    {
        method:"PUT",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a restructured to do",
            description: "new description",
            status: "completed",
            date_completed: '2022-08-26',
            userId: 999999
        })
    });
    
    const resJson = await response.json();
    console.log(resJson);
}
updateTodoStructure()

//10
async function patchTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            title: "this is a patched to do",
        })
    });
    
    const resJson = await response.json();
    console.log(resJson);
}
patchTodo();

//11
async function completeTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
    {
        method:"PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify({
            completed: true,
            date_completed: '2022-08-26'
        })
    });
    
    const resJson = await response.json();
    console.log(resJson);
}
completeTodo();

//12
async function deleteTodo(){
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/103',
    {
        method:"DELETE"
    });
    
    const resJson = await response.json();
    console.log(resJson);
    console.log('method deleted');
}
deleteTodo();